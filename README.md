# IG BvC Konformität von k8s-Clustern und Manifesten (Proof of Concept) test-policies

Eine Reihe von Richtlinien welche Kubernetes-Cluster und Manifeste mit einer Auswahl von Aspekten des BSI SYS1.6 - Container und Teilen der Ausarbeitungen der IG BvC kompatibel macht.

## Anleitung für Anwender
Für den Einsatz der Richtlinien im eigenen Cluster wird der Policy-Controller [Kyverno](https://kyverno.io/) benötigt. Die Installation von Kyverno ist unter [kyverno.io dokumentiert](https://kyverno.io/docs/installation/).

Auf der [Releases-Seite](https://gitlab.opencode.de/ig-bvc/richtlinien/-/releases) befinden sich die aktuellen Policies.
Die Richtlinien der IG BvC können direkt aus diesem Repository bezogen und angewendet werden. Alternativ kann ein Release-Bundle heruntergeladen werden.

# Dokumentation zum PoC

## Inhalt

- [Scope](#scope-definition)
- [Konformitätstest und Werkzeuge zur Durchsetzung von Richtlinien](#konformitätstest-und-werkzeuge-zur-durchsetzung-von-richtlinien)
- [Toolvergleich](#toolvergleich)
- [Erste Policies](#erste-policies)
- [Demos](#demos)

### Scope Definition

Nur auf Richtlinien und Konformitätstests konzentrieren, die auf der Kubernetes-API überprüfbar sind (In der folgenden Abbildung grün dargestellt 🟢). Obgleich viele wichtige Tests (z.B. der Infrastruktur wie bspw. Node-Konfiguration) damit erstmal unter den Tisch fallen, fokussieren wir uns der Zeit wegen hier.

![poc-scope.png](./docs/images/poc-scope.png)

### Konformitätstest und Werkzeuge zur Durchsetzung von Richtlinien

Richtlinien können automatisiert an drei Punkten des Anwendungslebenzyklus geprüft werden:

1. Während der Entwicklung (z.B. in CI-Pipelines): Statische Analyse der Manifeste mit Command-Line-Tools oder in Test-Clustern beim Entwickler.

🟢 In der folgenden Abbildung grün dargestellt.


2. Beim Deployment im Zielrechenzentrum: Prüfung via Admission-Controller

🟠 In der folgenden Abbildung orange dargestellt.


3. Im laufenden Betrieb: Kontinuierliche Prüfung auf bspw. manuelle Veränderung mit kubectl

🟠 In der folgenden Abbildung orange dargestellt.


Die Konformität des Clusters mit den Vorgaben der IG BvC kann mittels einem Konformitätstest (🟣) ermittelt werden.


![grob-konzept.png](./docs/images/grob-konzept.png)


### Toolvergleich

Der __Toolvergleich__ ist abgeschlossen und [dokumentiert](docs/tool-vergleich.md).


### Erste Policies

Wie es zu den __ersten Policies__ kommt, wurde in den [Quick-Wins](docs/quick-wins-poc.md) dokumentiert.


### Demos 

##### Statische Analyse von Manifesten

- [Workflow-Definition](.github/workflows/static-manifest-test.yml)

- [Run Workflow](.github/workflows/static-manifest-test.yml)

##### Richtlinien im Cluster durchsetzen test-policies-in-cluster

- [Workflow-Definition](.github/workflows/test-policies-in-cluster.yml)

- [Run Workflow](.github/workflows/test-policies-in-cluster.yml)

##### Cluster-Konformitäts-Test test-cluster-conformance

- [Workflow-Definition](.github/workflows/test-cluster-conformance.yml)

- [Run Workflow](.github/workflows/test-cluster-conformance.yml)
