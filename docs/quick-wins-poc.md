## Quick-Wins: Direkt, einfach und automatisiert durchsetzbare Richtlinien aus dem BSI SYS.1.6

### Basis-Anforderungen
- SYS.1.6.A5 Separierung der Container
  - Der Betriebssystem-Kernel MUSS über Namespaces (wie Linux cgroups) oder andere geeignete Mechanismen die Container voneinander und von anderen Prozessen auf dem Container-Host trennen. Die Trennung MUSS dabei mindestens Prozess-IDs, Inter-Process-Kommunikation, Benutzer-IDs, Dateisystem und Netz inklusive Hostname umfassen
  - https://github.com/kyverno/policies/blob/main/pod-security/baseline/disallow-host-namespaces/disallow-host-namespaces.yaml  (✔ Demo)
  - https://github.com/kyverno/policies/blob/main/pod-security/baseline/disallow-host-path/disallow-host-path.yaml
  - https://github.com/kyverno/policies/blob/main/pod-security/baseline/disallow-host-ports/disallow-host-ports.yaml
- SYS.1.6.A6 Verwendung sicherer Images 
  - Nur vertrauenswürdige registries (MUSS)
  - https://github.com/kyverno/policies/blob/main/best-practices/restrict_image_registries/restrict_image_registries.yaml (✔ Demo)
  - Versionsnummern (MUSS) ohne minor-version, um Updates nicht zu behindern (SOLLTE)
  - https://github.com/kyverno/policies/blob/main/best-practices/disallow_latest_tag/disallow_latest_tag.yaml

### Standard-Anforderungen
- SYS.1.6.A11 Richtlinie für Betrieb und Images
  - Unter anderem dieses Repo
- SYS.1.6.A14 Updates von Containern
  - Beim Start eines Containers SOLLTE der Container-Dienst immer auf die aktuell verfügbare Version des Images prüfen und eine vorhandene neue Version herunterladen. Auf dem Container-Host zwischengespeicherte alte Versionen DÜRFEN dann NICHT gestartet werden.
  - https://github.com/kyverno/policies/blob/main/other/imagepullpolicy-always.yaml (✔ Demo)
- SYS.1.6.A15 Unveränderlichkeit der Container
  - readOnlyRootFilesystem
  - https://github.com/kyverno/policies/blob/main/best-practices/require_ro_rootfs/require_ro_rootfs.yaml (✔ Demo)
- SYS.1.6.A21 Container-Ausführung ohne Privilegien
  - Alle Anwendungsdienste in Containern SOLLTEN nur unter einem nicht privilegierten Account gestartet werden. Sie SOLLTEN NICHT über erweiterte Privilegien für die Container-Dienste oder die Cluster-Betriebssoftware verfügen.
  - https://github.com/kyverno/policies/tree/main/pod-security/baseline/disallow-privileged-containers (✔ Demo)
- SYS.1.6.A25
  - Container SOLLTEN jeweils eigene Service-Accounts nutzen, um miteinander und mit den Diensten der Cluster-Betriebssoftware authentifiziert zu kommunizieren, Gruppen von Containern können, wenn sie gleiche Aufgaben haben, einen gemeinsamen Service-Account nutzen. Berechtigungen für die Service-Accounts SOLLTEN nur minimal vergeben sein.
    
    Jeder Dienst, der einen Service-Account nutzt, SOLLTE ein eigenes Token erhalten.
  - https://kyverno.io/policies/other/restrict_automount_sa_token/?policytypes=validate
  - https://kyverno.io/policies/other/restrict_service_account/?policytypes=validate
- SYS.1.6.A26 Accounts der Anwendungsdienste in Containern
  - Die Accounts der Prozesse in den Containern SOLLTEN keine Berechtigungen auf dem Container-Host haben. Wenn dies dennoch notwendig ist, SOLLTEN diese Berechtigungen nur für unbedingt notwendigen Daten gelten.
  - https://kyverno.io/policies/pod-security/restricted/deny-privilege-escalation/deny-privilege-escalation/?policytypes=validate
  - https://kyverno.io/policies/pod-security/restricted/require-run-as-nonroot/require-run-as-nonroot/?policytypes=validate
  - https://kyverno.io/policies/other/restrict_usergroup_fsgroup_id/?policytypes=validate
- SYS.1.6.A27
  - Jedes Image SOLLTE einen Health-Check für den Start und den Betrieb („readiness“ und „liveness“) definieren. Diese Checks SOLLTEN Auskunft über die Verfügbarkeit der Anwendung im Container geben. Sie SOLLTEN fehlschlagen, wenn die Anwendung nicht in der Lage ist, ihre Aufgaben ordnungsgemäß wahrzunehmen. 
 
    Der Container-Dienst oder die Cluster-Betriebssoftware SOLLTEN diese Checks überwachen und Container, bei denen die Checks fehlschlagen, beenden und durch neue Instanzen ersetzen.
  - https://kyverno.io/policies/best-practices/require_probes/require_probes/?policytypes=validate
  - https://kyverno.io/policies/other/ensure_probes_different/?policytypes=validate

### Anforderungen bei erhöhtem Schutzbedarf
- SYS.1.6.A30 Eigene Trusted Registry für Container
  - Images SOLLTEN nur in einem eigenen Verzeichnis (Registry) bereitgestellt werden. Es SOLLTE durch technische Maßnahmen sichergestellt sein, dass nur Images aus dieser Registry eingesetzt werden.
  - https://github.com/kyverno/policies/blob/main/best-practices/restrict_image_registries/restrict_image_registries.yaml
- SYS.1.6.A31 Erstellung erweiterter Richtlinien für Container
  - Erweiterte Richtlinien SOLLTEN die Berechtigungen der Container und der betriebenen Anwendungsdienste einschränken. Die Richtlinien SOLLTEN folgende Zugriffe einschränken:
    - Netzverbindungen,
      - https://kyverno.io/policies/other/require_netpol/?policytypes=validate
    - Dateisystem-Zugriffe und
    - Kernel-Anfragen (Syscalls)
      - https://kyverno.io/policies/pod-security/restricted/restrict-seccomp/restrict-seccomp/?policytypes=validate (?)
- SYS.1.6.A33 Mikro-Segmentierung von Containern
  - Die Container SOLLTEN nur über die notwendigen Netzports miteinander kommunizieren können. Es SOLLTEN innerhalb der virtuellen Netze Regeln existieren, die alle bis auf die für den Betrieb notwendigen Netzverbindungen unterbinden. Die Regeln SOLLTEN Quelle und Ziel der Verbindungen genau definieren und dafür mindestens die Service-Namen, Meta-Daten („Labels“) oder die Service-Accounts verwenden. Sofern möglich SOLLTEN die Regeln eine zertifikatsbasierte Authentifizierung vorschreiben und nur die in den Zertifikaten hinterlegten Identitäten für die Definition der erlaubten Verbindungen nutzen.
  
    Alle Kriterien, die als Bezeichnung für diese Verbindung dienen, SOLLTEN so abgesichert sein, dass nur berechtige Personen und Verwaltungs-Dienste diese Kriterien setzen dürfen. 
  - https://kyverno.io/policies/other/require_netpol/?policytypes=validate

